### Notes
---
- Gives Deathstrider an infinitely better titlemap and startup screen.
- Contains tips that are 100% guaranteed to be helpful.
- Not an April Fools joke at all.

### Credits
---
Graphical Assets:
- UndeadRyker (me)
	- Improved EVERYTHING about the titlemap.

Music:
- Krump Kings
	- Lightnin